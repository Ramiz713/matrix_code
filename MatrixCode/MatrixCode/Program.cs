﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MatrixCode
{

    public class MatrixItem
    {
        public int Value { get; set; }
        public MatrixItem Next { get; set; }
        public int Row { get; set; }
        public int Column { get; set; }
    }

    public class MatrixCode
    {
        MatrixItem head;
        MatrixItem tail;
        int dimension;

        public MatrixCode(int[][] matrix)
        {
            if (matrix == null) throw new ArgumentException();
            dimension = matrix.Count();
            for (int i = 0; i < dimension; i++)
                for (int j = 0; j < dimension; j++)
                {
                    if (matrix[i][j] == 0) continue;
                    if (head == null)
                        tail = head = CreateMatrixItem(i, j, matrix[i][j]);
                    else
                    {
                        var item = CreateMatrixItem(i, j, matrix[i][j]);
                        tail.Next = item;
                        tail = item;
                    }
                }
        }

        public int[][] Decode()
        {
            var newMatrix = new int[dimension][];
            for (int i = 0; i < dimension; i++)
                newMatrix[i] = new int[dimension];
            var current = head;
            while (current != null)
            {
                newMatrix[current.Row][current.Column] = current.Value;
                current = current.Next;
            }
            return newMatrix;
        }

        public void Insert(int i, int j, int value)
        {
            if (i >= dimension || j >= dimension) throw new ArgumentException();
            var current = head;
            while (current != null)
            {
                if (current.Row == i && current.Column == j)
                {
                    current.Value = value;
                    return;
                }
                current = current.Next;
            }
            tail.Next = CreateMatrixItem(i, j, value);
            tail = tail.Next;
        }

        public void Delete(int i, int j)
        {
            if (i >= dimension || j >= dimension) throw new ArgumentException();
            var previous = head;
            var current = head;
            while (current.Row != i || current.Column != j)
            {
                previous = current;
                current = current.Next;
            }
            if (current == head) head = head.Next;
            if (current == tail) tail = previous;
            previous.Next = current.Next;
        }

        public List<int> MinList()
        {                    
            if (head == null)
                return CreatingList(0, dimension);         
            var current = head;
            var minList = CreatingList(int.MaxValue, dimension);
            var column = 0;
            var count = 0;
            while (column < dimension)
            {
                if (column == current.Column)
                {
                    minList[column] = Math.Min(minList[column], current.Value);
                    count++;
                }                    
                current = current.Next;
                if (current == null)
                {
                    if (count != dimension)
                        minList[column] = Math.Min(minList[column], 0);
                    current = head;
                    column++;
                    count = 0;
                }                    
            }
            return minList;
        }

        List<int> CreatingList(int value, int size)
        {
            var list = new List<int>();
            for (int i = 0; i < size; i++)
                list.Add(value);
            return list;
        }

        public int DiagSum()
        {
            var current = head;
            var sum = 0;
            while (current != null)
            {
                if (current.Row == current.Column ||
                    current.Column == dimension - current.Row - 1)
                    sum += current.Value;
                current = current.Next;
            }
            return sum;
        }

        public void Transp()
        {
            var current = head;
            var buffer = 0;
            while (current != null)
            {
                buffer = current.Row;
                current.Row = current.Column;
                current.Column = buffer;
                current = current.Next;
            }
        }

        public void ColsSum(int j1, int j2)
        {

            var currentj1 = head;
            var currentj2 = head;
            while (currentj2 != null)
            {
                if (currentj2.Column == j2)
                {
                    while (currentj1 != null)
                    {
                        if (currentj1.Column == j1 && currentj2.Row == currentj1.Row) break;
                        currentj1 = currentj1.Next;
                    }
                    if (currentj1 == null)
                    {
                        currentj1 = CreateMatrixItem(currentj2.Row, j1, currentj2.Value);
                        tail.Next = currentj1;
                        tail = currentj1;
                    }
                    else currentj1.Value += currentj2.Value;
                    currentj1 = head;
                }
                currentj2 = currentj2.Next;
            }
        }

        MatrixItem CreateMatrixItem(int i, int j, int element)
        {
            return new MatrixItem
            {
                Value = element,
                Column = j,
                Row = i,
                Next = null
            };
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            int[][] bb =
                new int[][]
                {
                    new int[]{1, 2, 3},
                new int[]{0, -1, 6},
                new int[]{-3, 8, 9}
                };
            var codedMatrix = new MatrixCode(bb);
            ShowInConsole(codedMatrix);

            codedMatrix.Transp();
            Console.WriteLine("Transp");
            ShowInConsole(codedMatrix);

            codedMatrix.Insert(0, 0, 5);
            Console.WriteLine("Insert(0, 0, 5)");
            ShowInConsole(codedMatrix);

            codedMatrix.Insert(2, 2, -5);
            Console.WriteLine("Insert(2, 2, -5)");
            codedMatrix.Insert(0, 1, 3);
            Console.WriteLine("Insert(0, 1, 3)");
            ShowInConsole(codedMatrix);

            Console.WriteLine("ColsSum(1,2)");
            codedMatrix.ColsSum(1, 2);
            ShowInConsole(codedMatrix);

            Console.WriteLine("DiagSum: " + codedMatrix.DiagSum());
            Console.WriteLine();

            Console.WriteLine("Delete(1,2)");
            codedMatrix.Delete(1, 2);
            ShowInConsole(codedMatrix);

            var list = codedMatrix.MinList();
            Console.Write("MinList: ");
            foreach (var min in list)
                Console.Write(min + " ");
        }

        static void ShowInConsole(MatrixCode codedMatrix)
        {
            var matrix = codedMatrix.Decode();
            foreach (var row in matrix)
            {
                foreach (var element in row)
                    Console.Write(element + " ");
                Console.WriteLine();
            }
            Console.WriteLine();
        }
    }
}

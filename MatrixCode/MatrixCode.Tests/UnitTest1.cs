﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace MatrixCode.Tests
{
    [TestClass]
    public class MatrixCodeTest
    {
        static int[][] matrix =
                {
                new int[]{1, 2, 3},
                new int[]{0, -1, 6},
                new int[]{-3, 8, 9}
            };

        static int[][] transpMatrix =
                {
                new int[]{1, 0, -3},
                new int[]{2, -1, 8},
                new int[]{3, 6, 9},
            };

        static int[][] matrixWithZeros =
                {
                new int[]{0, 0, 0},
                new int[]{0, 0, 0},
                new int[]{0, 0, 0},
            };

        static int[][] matrixWithOneElement =
                {
                new int[]{ 101 }
            };

        static int[][] matrixNonZerosColumn =
                {
                new int[]{0, 0, 2},
                new int[]{0, 0, 3},
                new int[]{0, 0, 4},
            };


        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void CodeNullMatrix()
        {
            var codedMatrix = new MatrixCode(null);
        }

        [TestMethod]
        public void DecodeAfterDeletingAllElements()
        {
            var codedMatrix = new MatrixCode(matrixNonZerosColumn);
            codedMatrix.Delete(0, 2);
            codedMatrix.Delete(1, 2);
            codedMatrix.Delete(2, 2);
            var decodedMatrix = codedMatrix.Decode();
            for (int i = 0; i < codedMatrix.dimension; i++)
                for (int j = 0; j < codedMatrix.dimension; j++)
                    Assert.AreEqual(decodedMatrix[i][j], matrixWithZeros[i][j]);
        }

        [TestMethod]
        public void DecodeMatrix()
        {
            var codedMatrix = new MatrixCode(matrix);
            var decodedMatrix = codedMatrix.Decode();
            for (int i = 0; i < codedMatrix.dimension; i++)
                for (int j = 0; j < codedMatrix.dimension; j++)
                    Assert.AreEqual(decodedMatrix[i][j], matrix[i][j]);
        }

        [TestMethod]
        public void DecodeMatrixWithZeros()
        {
            var codedMatrix = new MatrixCode(matrixWithZeros);
            var decodedMatrix = codedMatrix.Decode();
            for (int i = 0; i < codedMatrix.dimension; i++)
                for (int j = 0; j < codedMatrix.dimension; j++)
                    Assert.AreEqual(decodedMatrix[i][j], matrixWithZeros[i][j]);
        }

        [TestMethod]
        public void DecodeMatrixWithOneElement()
        {
            var codedMatrix = new MatrixCode(matrixWithOneElement);
            var decodedMatrix = codedMatrix.Decode();
            for (int i = 0; i < codedMatrix.dimension; i++)
                for (int j = 0; j < codedMatrix.dimension; j++)
                    Assert.AreEqual(decodedMatrix[i][j], matrixWithOneElement[i][j]);
        }

        [TestMethod]
        public void InsertInMatrixWithOneElement()
        {
            var codedMatrix = new MatrixCode(matrix);
            codedMatrix.Insert(0, 0, 102);
            var decodedMatrix = codedMatrix.Decode();
            Assert.AreEqual(102, decodedMatrix[0][0]);
        }

        [TestMethod]
        public void InsertHead()
        {
            var codedMatrix = new MatrixCode(matrix);
            codedMatrix.Delete(0, 0);
            codedMatrix.Insert(0, 0, 10000);
            var decodedMatrix = codedMatrix.Decode();
            Assert.AreEqual(10000, decodedMatrix[0][0]);
        }

        [TestMethod]
        public void InsertTail()
        {
            var codedMatrix = new MatrixCode(matrix);
            codedMatrix.Delete(2, 2);
            codedMatrix.Insert(2, 2, 10000);
            var decodedMatrix = codedMatrix.Decode();
            Assert.AreEqual(10000, decodedMatrix[2][2]);
        }

        [TestMethod]
        public void InsertMiddleElement()
        {
            var codedMatrix = new MatrixCode(matrix);
            codedMatrix.Delete(0, 2);
            codedMatrix.Insert(0, 2, 10000);
            var decodedMatrix = codedMatrix.Decode();
            Assert.AreEqual(10000, decodedMatrix[0][2]);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void InsertElementWithIncorrectNumber()
        {
            var codedMatrix = new MatrixCode(matrix);
            codedMatrix.Insert(0, 7, 10000);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void DeleteElementWithIncorrectNumber()
        {
            var codedMatrix = new MatrixCode(matrix);
            codedMatrix.Insert(0, 7, 10000);
        }

        [TestMethod]
        public void DeleteMiddleElement()
        {
            var codedMatrix = new MatrixCode(matrix);
            codedMatrix.Delete(1, 1);
            var decodedMatrix = codedMatrix.Decode();
            Assert.AreEqual(false, matrix[1][1] == decodedMatrix[1][1]);
            Assert.AreEqual(0, decodedMatrix[1][1]);
        }

        [TestMethod]
        public void DeleteHead()
        {
            var codedMatrix = new MatrixCode(matrix);
            codedMatrix.Delete(0, 0);
            var decodedMatrix = codedMatrix.Decode();
            Assert.AreEqual(false, matrix[0][0] == decodedMatrix[0][0]);
            Assert.AreEqual(0, decodedMatrix[0][0]);
        }

        [TestMethod]
        public void DeleteTail()
        {
            var codedMatrix = new MatrixCode(matrix);
            codedMatrix.Delete(0, 0);
            var decodedMatrix = codedMatrix.Decode();
            Assert.AreEqual(false, matrix[0][0] == decodedMatrix[0][0]);
            Assert.AreEqual(0, decodedMatrix[0][0]);
        }

        [TestMethod]
        public void ColsSum()
        {
            var codedMatrix = new MatrixCode(matrix);
            codedMatrix.ColsSum(0, 2);
            var decodedMatrix = codedMatrix.Decode();
            Assert.AreEqual(matrix[0][0] + matrix[0][2], decodedMatrix[0][0]);
            Assert.AreEqual(matrix[1][0] + matrix[1][2], decodedMatrix[1][0]);
            Assert.AreEqual(matrix[2][0] + matrix[2][2], decodedMatrix[2][0]);
        }

        [TestMethod]
        public void ColsSumInMatrixWithZeros()
        {
            var codedMatrix = new MatrixCode(matrixWithZeros);
            codedMatrix.ColsSum(0, 2);
            var decodedMatrix = codedMatrix.Decode();
            Assert.AreEqual(matrixWithZeros[0][0] + matrixWithZeros[0][2], decodedMatrix[0][0]);
            Assert.AreEqual(matrixWithZeros[1][0] + matrixWithZeros[1][2], decodedMatrix[1][0]);
            Assert.AreEqual(matrixWithZeros[2][0] + matrixWithZeros[2][2], decodedMatrix[2][0]);
        }

        [TestMethod]
        public void ColsSumWhenOnlyOneColumnNonZero()
        {
            var codedMatrix = new MatrixCode(matrixNonZerosColumn);
            codedMatrix.ColsSum(0, 2);
            var decodedMatrix = codedMatrix.Decode();
            Assert.AreEqual(matrixNonZerosColumn[0][0] + matrixNonZerosColumn[0][2], decodedMatrix[0][0]);
            Assert.AreEqual(matrixNonZerosColumn[1][0] + matrixNonZerosColumn[1][2], decodedMatrix[1][0]);
            Assert.AreEqual(matrixNonZerosColumn[2][0] + matrixNonZerosColumn[2][2], decodedMatrix[2][0]);
        }

        [TestMethod]
        public void DiagSumInMatrixWithOneElement()
        {
            var codedMatrix = new MatrixCode(matrixWithOneElement);
            Assert.AreEqual(101, codedMatrix.DiagSum());
        }

        [TestMethod]
        public void DiagSumInMatrix()
        {
            var codedMatrix = new MatrixCode(matrix);
            Assert.AreEqual(9, codedMatrix.DiagSum());
        }

        [TestMethod]
        public void DiagSumInMatrixWithZeros()
        {
            var codedMatrix = new MatrixCode(matrixWithZeros);
            Assert.AreEqual(0, codedMatrix.DiagSum());
        }

        [TestMethod]
        public void MinList()
        {
            var codedMatrix = new MatrixCode(matrix);
            var list = codedMatrix.MinList();
            Assert.AreEqual(-3, list[0]);
            Assert.AreEqual(-1, list[1]);
            Assert.AreEqual(3, list[2]);
        }

        [TestMethod]
        public void MinListInMatrixWithOneElement()
        {
            var codedMatrix = new MatrixCode(matrixWithOneElement);
            var list = codedMatrix.MinList();
            Assert.AreEqual(101, list[0]);
        }

        [TestMethod]
        public void MinListInMatrixWithZeros()
        {
            var codedMatrix = new MatrixCode(matrixWithZeros);
            var list = codedMatrix.MinList();
            Assert.AreEqual(0, list[0]);
            Assert.AreEqual(0, list[1]);
            Assert.AreEqual(0, list[2]);
        }

        [TestMethod]
        public void MinListAfterTrans()
        {
            var codedMatrix = new MatrixCode(matrix);
            codedMatrix.Transp();
            var list = codedMatrix.MinList();
            Assert.AreEqual(1, list[0]);
            Assert.AreEqual(-1, list[1]);
            Assert.AreEqual(-3, list[2]);
        }

        [TestMethod]
        public void Transp()
        {
            var codedMatrix = new MatrixCode(matrix);
            codedMatrix.Transp();
            var decodedMatrix = codedMatrix.Decode();
            for (int i = 0; i < codedMatrix.dimension; i++)
                for (int j = 0; j < codedMatrix.dimension; j++)
                    Assert.AreEqual(decodedMatrix[i][j], transpMatrix[i][j]);
        }

        [TestMethod]
        public void TranspMatrixWithZeros()
        {
            var codedMatrix = new MatrixCode(matrixWithZeros);
            codedMatrix.Transp();
            var decodedMatrix = codedMatrix.Decode();
            for (int i = 0; i < codedMatrix.dimension; i++)
                for (int j = 0; j < codedMatrix.dimension; j++)
                    Assert.AreEqual(decodedMatrix[i][j], matrixWithZeros[i][j]);
        }

        [TestMethod]
        public void Transp2X()
        {
            var codedMatrix = new MatrixCode(matrix);
            codedMatrix.Transp();
            codedMatrix.Transp();
            var decodedMatrix = codedMatrix.Decode();
            for (int i = 0; i < codedMatrix.dimension; i++)
                for (int j = 0; j < codedMatrix.dimension; j++)
                    Assert.AreEqual(decodedMatrix[i][j], matrix[i][j]);
        }

    }
}
